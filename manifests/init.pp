# Class= Test
#

class test (
  $class_param_one   = 'class_param_one default param value',
  $class_param_two   = 'class_param_two default param value',
  $class_param_three = 'class_param_three default param value',
  $class_param_four  = 'class_param_four default param value',
  $class_param_five  = 'class_param_five default param value',
  $class_param_six  = 'class_param_six default param value',
  ){
  warning('This is a test module. Do Not Use in production')

  notify { 'Class Param one': message => $class_param_one }
  notify { 'Class Param two': message => $class_param_two }
  notify { 'Class Param three': message => $class_param_three }
  notify { 'Class Param four': message => $class_param_four }
  notify { 'Class Param five': message => $class_param_five }
  notify { 'Class Param six': message => $class_param_six }
}
